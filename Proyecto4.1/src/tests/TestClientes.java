package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestClientes {
	static	GestorContabilidad gestorPruebas;
	Cliente actual;
	
	@BeforeClass
	public static void inicializarClase(){
		gestorPruebas = new GestorContabilidad();
	}
	
	//busco un cliente que no existe sin lista de clientes
	@Test
	public void testBuscarClienteInexistenteSinClientes() {
		//Busco un dni que no exista, debe devolver null
		String dni = "2345234";
		
		Cliente actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}
	
	// busco cliente que existe 
	@Test
	public void testBuscarClienteExistente(){
		Cliente esperado = new Cliente("1234F", "Fernando", LocalDate.now());
		gestorPruebas.getListaClientes().add(esperado);
		actual = gestorPruebas.buscarCliente("1234F");
		assertSame(esperado, actual);
	}
	
	//busco un cliente que no est� en la lista de clientes
	@Test
	public void testBuscarClienteInexistenteConClientes(){	
		String dni = "64F";
		//Busco un objeto que no exista, el resultado debe ser null
		actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}
	
	//buscamos  un cliente dentro de una lista con varios clientes
	@Test
	public void testBuscarClienteHabiendoVariosClientes(){
		Cliente esperado = new Cliente("34567F", "Maria", LocalDate.parse("1990-05-04"));
		gestorPruebas.getListaClientes().add(esperado);
		String dniABuscar = "1234678L";
		esperado = new Cliente(dniABuscar, "Pedro", LocalDate.parse("1995-07-02"));
		gestorPruebas.getListaClientes().add(esperado);
		
		actual = gestorPruebas.buscarCliente(dniABuscar);
		
		assertSame(esperado, actual);
	}
	
	//buscamos el cliente m�s antiguo
	@Test
	public void testClienteMasAntiguo() {
		Cliente cliente1 = new Cliente("34567F", "Maria", LocalDate.parse("1990-05-04"));
		Cliente cliente2= new Cliente("1234F", "Momath", LocalDate.now());
		gestorPruebas.getListaClientes().add(cliente1);
		gestorPruebas.getListaClientes().add(cliente2);
		Cliente actual=gestorPruebas.clienteMasAntiguo();
		assertEquals(cliente1, actual);
	}
	
	//eliminaremos un cliente existente
		@Test
		public void eliminarClienteexistente(){
			Cliente clienteAEliminar = new Cliente("1234F", "Momath", LocalDate.now());
			gestorPruebas.getListaClientes().add(clienteAEliminar);
			gestorPruebas.eliminarCliente("1234F");
			boolean erro=gestorPruebas.getListaClientes().contains(clienteAEliminar);
			assertFalse(erro);
		}
	
	
}
