package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;

class TestAltaClientes {
	

	static	GestorContabilidad gestorPruebas;
	Cliente actual;
	
	@BeforeClass
	public static void inicializarClase(){
		gestorPruebas = new GestorContabilidad();
	}

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		fail("Not yet implemented");
	}
	
	//damos de alta a un cliente
	@Test
	public void testAltaPrimerCliente() {
		Cliente nuevoCliente=new Cliente("123456","Pablo", LocalDate.parse("2010-05-10"));
		
		gestorPruebas.altaCliente(nuevoCliente);
		
		boolean actual=gestorPruebas.getListaClientes().contains(nuevoCliente);
		
		assertTrue(actual);
	}
	
	//damos de alta a varios clientes
	@Test
	public void testAltaVariosClientes() {
		Cliente newCliente=new Cliente("7474J","ANA", LocalDate.parse("2016-07-08"));
		gestorPruebas.altaCliente(newCliente);
		
		Cliente otroCliente=new Cliente("8563G","MOMATH", LocalDate.parse("2014-04-09"));
		gestorPruebas.altaCliente(newCliente);
		
		boolean actual=gestorPruebas.getListaClientes().contains(newCliente);
		boolean actual2=gestorPruebas.getListaClientes().contains(otroCliente);
		assertTrue(actual);
		assertTrue(actual2);
	}
	
	//damos de alta un cliente con un dni repetido
	@Test
	public void testAltaClienteDniRepetido() {
		Cliente nuevoCliente=new Cliente("123456","Pablo", LocalDate.parse("2010-05-10"));
		
		gestorPruebas.altaCliente(nuevoCliente);
		
		Cliente otroCliente=new Cliente("123456","Jos�", LocalDate.parse("2010-05-10"));
		
		boolean actual=gestorPruebas.getListaClientes().contains(nuevoCliente);
		
		assertTrue(actual);
	}
}
