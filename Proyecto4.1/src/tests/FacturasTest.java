package tests;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

import static java.lang.Math.*;
class FacturasTest {
	static Factura facturaPrueba=new Factura();
	
	
	@BeforeClass
	public static void prepararClasePruebas() {
		facturaPrueba =new Factura();
	}
	
	
	static	GestorContabilidad gestorFactura;
	Factura actual;
	
	@BeforeClass
	public static void inicializarClase(){
		gestorFactura = new GestorContabilidad();
	}
	
		@Test
		//fail("Not yet implemented");
		 void testCalcuclarPrecioProducto1() {
			facturaPrueba.setCantidad(3);
			facturaPrueba.setPrecioUnidad(2.5);
			
			double esperado =7.5;
			double actual=facturaPrueba.calcularPrecioTotal();
			
			assertEquals(esperado,actual);
		}
		
		//calcula una cantidad que da 0
		@Test
		 void testCalcularCantidadCero() {
			facturaPrueba.setCantidad(0);
			facturaPrueba.setPrecioUnidad(2.5);
			
			double esperado =0;
			double actual=facturaPrueba.calcularPrecioTotal();
			
			assertEquals(esperado,actual);
			
		}
		
		//calculamos un precio negativo
		@Test
		void testCalcularPrecioConPrecioNegativo() {
			facturaPrueba.setCantidad(2);
			facturaPrueba.setPrecioUnidad(-1.5);
			
			double esperado =-3;
			double actual=facturaPrueba.calcularPrecioTotal();
			
			assertEquals(esperado,actual);
			
		}
		
		//buscamos una factura que no existe
		@Test 
		void testBuscarFacturaInexistente() {
			String codigoFactura = "477G";
			Factura actual = gestorFactura.buscarFactura(codigoFactura);
			assertNull(actual);
		}
		
		//buscamos una factura que existe
		@Test
		void BuscarFacturaExistente() {
			Factura esperado = new Factura("750L4", LocalDate.now());
			gestorFactura.getListaFacturas().add(esperado);
			actual = gestorFactura.buscarFactura("750L4");
			assertSame(esperado, actual);
		}
		
		//creamos una factura que est� repetida
		@Test
		void testCrearFacturaRepetido() {
			Factura nuevaFactura1 = new Factura();
			nuevaFactura1.setCodigoFactura("750L4");
			gestorFactura.getListaFacturas().add(nuevaFactura1);
			Factura nuevaFactura2= new Factura();
			nuevaFactura2.setCodigoFactura("750L4");
			gestorFactura.crearFactura(nuevaFactura2);
			boolean actual = gestorFactura.getListaFacturas().contains(nuevaFactura2);
			assertFalse(actual);
		}
		
		//creamos una nueva factura
		@Test
		void testCrearFactura() {
			Factura nuevaFactura11=new Factura();
			nuevaFactura11.setCodigoFactura("864IW");
			gestorFactura.crearFactura(nuevaFactura11);
			boolean actual=gestorFactura.getListaFacturas().contains(nuevaFactura11);
			assertTrue(actual);
		}
		
		//buscamos la factura mas antigua
		@Test 
		void testFacturaMasCara() {
			Factura unaFactura=new Factura();
			
		}
		
		//factura mas cara nula
		@Test
		public void facturaMasCaranula(){
			Factura esperado=gestorFactura.facturaMasCara();
			assertNull(esperado);
		}
		
		//buscamos la facturacion anual
		@Test
		public void facturacionAnual() {
			Factura factura1= new Factura(5, 4,"2016");
			Factura factura2= new Factura(3, 8,"2016");
			
			gestorFactura.getListaFacturas().add(factura1);
			gestorFactura.getListaFacturas().add(factura2);
			float esperado= gestorFactura.calcularFacturacionAnual("1990");
			float actual=12;
			assertEquals(esperado, actual, 0);
		}
		
		
		
}
